FROM docker.io/library/ubuntu:22.04 AS base

RUN apt-get -y update && \
    apt-get -y install jq bash perl && \
    apt-get -y clean && \
    apt-get -y autoremove


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/

RUN  perl -e 'exit 0' \
     && jq -c 'select(.originalJobPosting.scraper == "megascrape")' < /testdata/input.10 > output.1 \
     && diff /testdata/expected_output.1 output.1 \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD ["/usr/bin/jq"]
